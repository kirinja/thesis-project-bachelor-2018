package vr.window;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;

public class StreamingActivity extends Activity
{
    public static final int REQUEST_CAMERA_PERMISSION = 200;


    private LoopThread loopThread;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_streaming);

        loopThread = new LoopThread(this);
    }


    @Override
    protected void onResume()
    {
        super.onResume();
        loopThread.activate();
    }


    @Override
    protected void onPause()
    {
        super.onPause();
        loopThread.finish();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == REQUEST_CAMERA_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED)
        {
            loopThread.activate();
        }
        else if(requestCode == REQUEST_CAMERA_PERMISSION)
        {
            finish();
        }
    }
}
