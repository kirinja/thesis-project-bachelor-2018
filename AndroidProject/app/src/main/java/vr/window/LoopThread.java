package vr.window;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.TotalCaptureResult;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.view.Surface;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

public class LoopThread extends Thread
{
//region CONSTANTS

    private static final int FPS = 30;
    private static final int WIDTH = 128;
    private static final int HEIGHT = 128;
    private static final String IP_ADDRESS = "192.168.0.100";
    private static final int PORT = 12345;

//endregion


//region VARIABLES

    private StreamingActivity activity;

    private boolean isRunning = false;
    private Handler backgroundHandler;
    private HandlerThread backgroundThread;
    private CountDownLatch syncLatch = new CountDownLatch(1);

    private CameraDevice cameraDevice;
    private CaptureRequest captureRequest;
    private CameraCaptureSession captureSession;

    private ArrayList<Surface> surfaces = new ArrayList<>();
    private ImageReader imageReader;

    private int frameNumber;

    private DatagramSocket socket;
    private InetAddress address;

//endregion


//region PUBLIC

    public LoopThread(StreamingActivity activity)
    {
        this.activity = activity;
    }


    public void activate()
    {
        if(!checkCameraPermission())
        {
            return;
        }

        startBackgroundThread();
        try
        {
            createImageReader();
            openCamera();
        } catch (Exception e)
        {
            e.printStackTrace();
            releaseResources();
        }
    }


    public void finish()
    {
        isRunning = false;
    }


    @Override
    public void run()
    {
        try
        {
            initNetwork();
            runLoop();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            releaseResources();
        }
    }

//endregion


//region INIT

    private boolean checkCameraPermission()
    {
        if (activity.checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            activity.requestPermissions(new String[]{Manifest.permission.CAMERA}, StreamingActivity.REQUEST_CAMERA_PERMISSION);
            return false;
        }
        return true;
    }


    private void startBackgroundThread()
    {
        backgroundThread = new HandlerThread("Camera Background");
        backgroundThread.start();
        backgroundHandler = new Handler(backgroundThread.getLooper());
    }


    private void createImageReader()
    {
        imageReader = ImageReader.newInstance(WIDTH, HEIGHT, ImageFormat.YUV_420_888, 2);
        surfaces.add(imageReader.getSurface());

        imageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener()
        {
            @Override
            public void onImageAvailable(ImageReader imageReader)
            {
            }
        }, backgroundHandler);
    }


    private void openCamera() throws IllegalAccessException, CameraAccessException
    {
        if (activity.checkSelfPermission(Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED)
        {
            throw new IllegalAccessException("Need camera permission to open camera!");
        }

        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        String cameraID = manager.getCameraIdList()[0];
        System.out.println("Attempting to open camera");
        manager.openCamera(cameraID, new CameraDevice.StateCallback()
        {
            @Override
            public void onOpened(CameraDevice camera)
            {
                System.out.println("Opened camera");
                cameraDevice = camera;
                try
                {
                    startCaptureSession();
                }
                catch (CameraAccessException e)
                {
                    e.printStackTrace();
                    releaseResources();
                }
            }

            @Override
            public void onDisconnected(CameraDevice camera)
            {
            }

            @Override
            public void onError(CameraDevice camera, int i)
            {
            }
        }, backgroundHandler);
    }


    private void startCaptureSession() throws CameraAccessException
    {
        CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_RECORD);
        captureBuilder.addTarget(surfaces.get(0));
        captureBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        captureRequest = captureBuilder.build();

        cameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback()
        {
            @Override
            public void onConfigured(CameraCaptureSession cameraCaptureSession)
            {
                captureSession = cameraCaptureSession;
                start();
            }

            @Override
            public void onConfigureFailed(CameraCaptureSession cameraCaptureSession)
            {
            }
        }, backgroundHandler);
    }


    private void initNetwork() throws IOException
    {
        address = InetAddress.getByName(IP_ADDRESS);
        socket = new DatagramSocket(PORT);
    }

//endregion


    private void releaseResources()
    {
        if(socket != null)
        {
            socket.close();
            socket = null;
        }

        if(captureSession != null)
        {
            captureSession.close();
            captureSession = null;
        }

        if (cameraDevice != null)
        {
            cameraDevice.close();
            cameraDevice = null;
        }

        if(imageReader != null)
        {
            imageReader.close();
            imageReader = null;
        }

        stopBackgroundThread();
    }


    private void stopBackgroundThread()
    {
        backgroundThread.quitSafely();
        try
        {
            backgroundThread.join();
            backgroundThread = null;
            backgroundHandler = null;
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }


    private void runLoop()
    {
        isRunning = true;
        while (isRunning)
        {
            long time = System.currentTimeMillis();

            update();

            long frameTime = System.currentTimeMillis() - time;
            long sleepTime = 1000 / FPS - frameTime;
            if (sleepTime > 0)
            {
                try
                {
                    Thread.sleep(sleepTime);
                } catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }


    private void update()
    {
        takePicture();
        try
        {
            syncLatch.await();
            syncLatch = new CountDownLatch(1);
            sendData();
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }


    private void takePicture()
    {
        System.out.println("Taking picture");
        try
        {
            captureSession.capture(captureRequest, new CameraCaptureSession.CaptureCallback()
            {
                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result)
                {
                    syncLatch.countDown();
                }
            }, backgroundHandler);
        } catch (CameraAccessException e)
        {
            e.printStackTrace();
        }
    }


    private void sendData()
    {
        System.out.println("Attempting to send data");
        try
        {
            Image image = imageReader.acquireLatestImage();
            byte[] bytes = bytesFromImage(image, WIDTH, HEIGHT);
            image.close();

            sendFrame(bytes);
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }


    private void sendFrame(byte[] frame)
    {
        int packetSizeLimit = 508;
        int headerSize = 12;
        int dataSizeLimit = packetSizeLimit - headerSize;
        try
        {
            int packets = (int) Math.ceil(frame.length / (double) dataSizeLimit);
            for(int packetNumber = 0; packetNumber < packets; ++packetNumber)
            {
                int startOffset = packetNumber * dataSizeLimit;
                ByteBuffer buffer = ByteBuffer.allocate(Math.min(packetSizeLimit, frame.length - startOffset + headerSize));
                buffer.order(ByteOrder.LITTLE_ENDIAN);
                buffer.putInt(frameNumber);
                buffer.putInt(packetNumber);
                buffer.putInt(frame.length);
                buffer.put(frame, startOffset, Math.min(dataSizeLimit, frame.length - startOffset));
                byte[] packetData = buffer.array();
                DatagramPacket packet = new DatagramPacket(packetData, packetData.length, address, PORT);
                socket.send(packet);
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        ++frameNumber;
    }


    private static byte[] bytesFromImage(Image image, int width, int height)
    {
        int totalSize = width * height;
        byte[] arr = new byte[totalSize * 3 / 2];

        Image.Plane[] planes = image.getPlanes();
        ByteBuffer yBuf = planes[0].getBuffer();
        ByteBuffer uBuf = planes[1].getBuffer();
        ByteBuffer vBuf = planes[2].getBuffer();
        for(int y = 0; y < height; ++y)
        {
            for(int x = 0; x < width; ++x)
            {
                arr[y * width + x] = yBuf.get(y * planes[0].getRowStride() + x);
                if(x % 2 == 0 && y % 2 == 0)
                {
                    arr[totalSize + (y / 2) * (width / 2) + x / 2] = uBuf.get(y / 2 * planes[1].getRowStride() + (x / 2) * planes[1].getPixelStride());
                    arr[totalSize + totalSize / 4 + (y / 2) * (width / 2) + x / 2] = vBuf.get(y / 2 * planes[2].getRowStride() + (x / 2) * planes[2].getPixelStride());
                }
            }
        }

        return arr;
    }
}
