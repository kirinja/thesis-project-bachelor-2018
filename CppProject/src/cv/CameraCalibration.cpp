#include <ChessboardDetection.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/shape/hist_cost.hpp>


using namespace std::string_literals;


void calibrate_camera()
{
	auto calibrator = ChessboardDetection::Camera_calibrator{};

	const auto w = 5;
	const auto h = 9;
	const auto num_images = 12;
	const auto file_name = "chess_calibration_"s;
	const auto square_size = 0.0097778f;

	for(auto i = 0; i < num_images; ++i)
	{
		const auto img = cv::imread(file_name + std::to_string(i) + ".jpg"s, cv::IMREAD_COLOR);
		calibrator.add_image(img);
	}

	calibrator.calibrate({ w, h }, square_size);
}

int main()
{
	calibrate_camera();
}
