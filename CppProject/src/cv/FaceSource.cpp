#include <LibIncludes.hpp>

// http://thomasmountainborn.com/2017/03/05/unity-and-opencv-part-three-passing-detection-data-to-unity/

cv::CascadeClassifier _faceCascade;
cv::String _windowName = "Unity OpenCV Interop Sample";
//cv::VideoCapture _capture;
int _scale = 1;
cv::Mat _image;

cv::Mat _unityImage;

extern "C" int __declspec(dllexport) __stdcall Init(int& outCameraWidth, int& outCameraHeight)
{
	// Load LBP face cascade.
	if (!_faceCascade.load("lbpcascade_frontalface.xml"))
		return -1;

	// Open the stream.
	//_capture.open(0);
	//if (!_capture.isOpened())
		//return -2;

	// Load image
	_image = cv::imread("test2.png", cv::IMREAD_COLOR);
	if (!_image.data)
		return -2;

	//outCameraWidth = _capture.get(cv::CAP_PROP_FRAME_WIDTH);
	//outCameraHeight = _capture.get(cv::CAP_PROP_FRAME_HEIGHT);

	outCameraWidth = _image.size().width;
	outCameraHeight = _image.size().height;

	return 0;
}

extern "C" void __declspec(dllexport) __stdcall Close()
{
	_image.release();
	//_capture.release();
	cv::destroyAllWindows();
}


extern "C" void __declspec(dllexport) __stdcall Set_Scale(int scale)
{
	_scale = scale;
}

extern "C" void __declspec(dllexport) __stdcall Detect(Circle* outFaces, int maxOutFacesCount, int& outDetectedFacesCount)
{
	cv::Mat frame;
	//_capture >> frame;
	frame = _image;
	if (frame.empty())
		return;

	std::vector<cv::Rect> faces;
	// Convert the frame to grayscale for cascade detection.
	cv::Mat grayscaleFrame;
	cv::cvtColor(frame, grayscaleFrame, cv::COLOR_BGR2GRAY);
	cv::Mat resizedGray;
	// Scale down for better performance.
	cv::resize(grayscaleFrame, resizedGray, cv::Size(frame.cols / _scale, frame.rows / _scale));
	cv::equalizeHist(resizedGray, resizedGray);

	// Detect faces.
	_faceCascade.detectMultiScale(resizedGray, faces);

	// Draw faces.
	for (size_t i = 0; i < faces.size(); i++)
	{
		cv::Point center(_scale * (faces[i].x + faces[i].width / 2), _scale * (faces[i].y + faces[i].height / 2));
		cv::ellipse(frame, center, cv::Size(_scale * faces[i].width / 2, _scale * faces[i].height / 2), 0, 0, 360, cv::Scalar(0, 0, 255), 4, 8, 0);

		// Send to application.
		outFaces[i] = Circle(faces[i].x, faces[i].y, faces[i].width / 2);
		outDetectedFacesCount++;

		if (outDetectedFacesCount == maxOutFacesCount)
			break;
	}

	cv::imshow(_windowName, frame);
}

// this creates an image from unity data (such as byte array, image width and height)
extern "C" void __declspec(dllexport) __stdcall Send_ByteData(ByteData* inData, int width, int height, int dataLength)
{
	unsigned char *arr = new unsigned char[dataLength];
	for (size_t i = 0; i < dataLength; i++)
	{
		arr[i] = inData[i].Byte;
	}

	//cv::String test = std::to_string(bytes.size());
	cv::String test = "Image data from Unity";

	// recieve a height by width texture that is 4 channeled ( example: 512 * 512 * 4)
	cv::Mat m(height, width, CV_8UC4, arr);

	// are all these flippings neccessary?
	cv::flip(m, _unityImage, 0);

	cv::imshow(test, _unityImage);
}

// copies the image bytes so unity can access it later (the image is flipped so need to add cv::flip before we send the data)
// this sends the current image stored in _image to Unity
extern "C" void __declspec(dllexport) __stdcall Process_Frame(unsigned char* *data, int *size, int *width, int *height)
{
	cv::Mat image = _image;
	cv::Mat betterImage;
	cv::Mat finImage;

	cv::cvtColor(image, betterImage, CV_BGR2BGRA, 4);

	// are all these flippings neccessary?
	cv::flip(betterImage, finImage, 1);

	unsigned char* result;
	result = new unsigned char[finImage.cols * finImage.rows * 4];

	memcpy(result, finImage.data, finImage.cols * finImage.rows * 4);
	*size = finImage.cols * finImage.rows * 4;

	*width = finImage.cols;
	*height = finImage.rows;

	*data = result;
}

// this function should be used on the _unityImage to detect the different objects in the scene (espcially the mobile phone) and give us data about it, such as position etc
extern "C" void __declspec(dllexport) __stdcall Detect_Object()
{
	// declare local variables
	cv::Rect2d roi;
	cv::Mat frame;

	
}