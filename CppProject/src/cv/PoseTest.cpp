#include <ChessboardDetection.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videostab.hpp>
#include <opencv2/opencv.hpp>
#include <chrono>


using namespace std::string_literals;


void draw(cv::Mat& img, std::vector<cv::Point2f>& imgpts)
{
	cv::line(img, imgpts[0], imgpts[1], { 255, 0, 0 }, 5);
	cv::line(img, imgpts[0], imgpts[2], { 0, 255, 0 }, 5);
	cv::line(img, imgpts[0], imgpts[3], { 0, 0, 255 }, 5);
}


void find_poses()
{
	cv::Mat camera_matrix;
	cv::Mat distortion_coefficients;

	auto fs = cv::FileStorage{ "calib_data.yml"s, cv::FileStorage::READ };
	fs["camera_matrix"] >> camera_matrix;
	fs["distortion_coefficients"] >> distortion_coefficients;
	fs.release();

	const auto w = 5;
	const auto h = 9;
	const auto num_images = 12;
	const auto file_name = "chess_calibration_"s;
	const auto square_size = 0.0097778f;

	auto detector = ChessboardDetection::Detector{ camera_matrix, distortion_coefficients, {w, h}, square_size };

	const auto axis = std::vector<cv::Point3f>{ {0, 0, 0}, { 0.0097778f * 3, 0, 0 },{ 0, 0.0097778f * 3, 0 },{ 0, 0, -0.0097778f * 3 } };

	auto total_dur = long long{};
	for(auto i = 0; i < num_images; ++i)
	{
		auto img = cv::imread(file_name + std::to_string(i) + ".jpg"s, cv::IMREAD_COLOR);
		auto rot = cv::Mat{};
		auto trans = cv::Mat{};

		const auto start_time = std::chrono::high_resolution_clock::now();
		if(detector.detect(img, rot, trans))
		{
			const auto end_time = std::chrono::high_resolution_clock::now();
			auto dur = end_time - start_time;
			total_dur += dur.count();
			auto imgpts = std::vector<cv::Point2f>{};
			cv::projectPoints(axis, rot, trans, camera_matrix, distortion_coefficients, imgpts);

			draw(img, imgpts);

			cv::imshow("img"s, img);
			cv::moveWindow("img"s, 0, 0);
			cv::waitKey(0);
		}
		else
		{
			std::cout << "Could not find pose in image no "s << i << std::endl;
		}
	}
	const auto avg_dur = total_dur / num_images;
	std::cout << "Average detection time: "s << avg_dur << " nanoseconds ("s << avg_dur / 1000000.0 << " milliseconds)"s << std::endl;
	cv::waitKey(0);
}

int main()
{
	find_poses();
}
