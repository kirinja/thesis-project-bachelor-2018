#include "ChessboardDetection.hpp"
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/shape.hpp>
#include <string>
#include <opencv2/highgui.hpp>
#include <utility>


using namespace std::string_literals;


namespace ChessboardDetection
{
	Detector::Detector(cv::Mat camera_matrix, cv::Mat distortion_coefficients, cv::Size chessboard_inner_corners, const float square_size)
		: camera_matrix_{std::move(camera_matrix)}, distortion_coefficients_{std::move(distortion_coefficients)}, board_size_{chessboard_inner_corners}
	{
		for (auto y = 0; y < board_size_.height; ++y)
		{
			for (auto x = 0; x < board_size_.width; ++x)
			{
				object_points_.emplace_back(x * square_size - board_size_.width * square_size / 2, y * square_size - board_size_.height * square_size / 2, 0.0f);
			}
		}
	}


	bool Detector::detect(const cv::Mat& image, cv::Mat& rotation_vector, cv::Mat& translation_vector) const
	{
		const auto term_criteria = cv::TermCriteria{ cv::TermCriteria::EPS + cv::TermCriteria::MAX_ITER, 30, 0.001 };

		auto corners = std::vector<cv::Point2f>{};
		auto gray = cv::Mat{ image.rows, image.cols, image.type() };
		cv::cvtColor(image, gray, cv::COLOR_BGR2GRAY);
		if (findChessboardCorners(gray, { board_size_.width, board_size_.height }, corners))
		{
			cornerSubPix(gray, corners, { 11, 11 }, { -1, -1 }, term_criteria);

			return cv::solvePnPRansac(object_points_, corners, camera_matrix_, distortion_coefficients_, rotation_vector, translation_vector);
		}

		return false;
	}


	const cv::Mat& Detector::camera_matrix() const
	{
		return camera_matrix_;
	}


	const cv::Mat& Detector::distortion_coefficients() const
	{
		return distortion_coefficients_;
	}


	void Camera_calibrator::add_image(cv::Mat image)
	{
		images_.push_back(std::move(image));
	}


	void Camera_calibrator::calibrate(const cv::Size chessboard_inner_corners, const float square_size)
	{
		const auto term_criteria = cv::TermCriteria{ cv::TermCriteria::EPS + cv::TermCriteria::MAX_ITER, 30, 0.001 };
		const auto w = chessboard_inner_corners.width;
		const auto h = chessboard_inner_corners.height;

		auto obj_points = std::vector<cv::Point3f>{};
		for (auto y = 0; y < h; ++y)
		{
			for (auto x = 0; x < w; ++x)
			{
				obj_points.emplace_back(x * square_size - w * square_size / 2, y * square_size - h * square_size / 2, 0.0f);
			}
		}

		auto obj_points_all = std::vector<std::vector<cv::Point3f>>{};
		auto img_points = std::vector<std::vector<cv::Point2f>>{};
		for (auto& image : images_)
		{
			auto corners = std::vector<cv::Point2f>{};
			auto img = cv::Mat{};
			cv::cvtColor(image, img, cv::COLOR_BGR2GRAY);
			if (findChessboardCorners(img, { w, h }, corners))
			{
				obj_points_all.push_back(obj_points);
				cornerSubPix(img, corners, { 11, 11 }, { -1, -1 }, term_criteria);
				img_points.push_back(corners);

				drawChessboardCorners(image, { w, h }, corners, true);
				cv::imshow("img"s, image);
				cv::waitKey(0);
			}
		}

		cv::destroyAllWindows();

		auto& img = images_[0];
		const auto img_size = cv::Size{ img.cols, img.rows };

		cv::Mat camera_matrix;
		cv::Mat distortion_coefficients;
		std::vector<cv::Mat> rotation_vectors;
		std::vector<cv::Mat> translation_vectors;
		cv::calibrateCamera(obj_points_all, img_points, img_size, camera_matrix, distortion_coefficients, rotation_vectors, translation_vectors);

		auto roi = cv::getOptimalNewCameraMatrix(camera_matrix, distortion_coefficients, img_size, 1.0, img_size);

		auto undistorted = cv::Mat{ img.rows, img.cols, img.type() };
		cv::undistort(img, undistorted, camera_matrix, distortion_coefficients);
		cv::imwrite("calib_result.png", undistorted);

		auto fs = cv::FileStorage{ "calib_data.yml"s, cv::FileStorage::WRITE };
		fs << "camera_matrix"s << camera_matrix;
		fs << "distortion_coefficients"s << distortion_coefficients;
	}
}
