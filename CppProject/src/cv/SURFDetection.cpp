#include <LibIncludes.hpp>
#include <SURFDetection.hpp>

namespace SURFDetection
{
	// https://docs.opencv.org/3.3.1/d7/dff/tutorial_feature_homography.html
	bool surf_image_recognition(ByteData* in_scene, int scene_w, int scene_h, int scene_length,
		ByteData* in_object, int obj_w, int obj_h, int object_length, RectanglePoints* out_points)
	{
		auto obj_vec = std::vector<uint8_t>(object_length);
		for (auto i = 0; i < object_length; ++i)
		{
			obj_vec.push_back(in_object[i].Byte);
		}

		auto scene_vec = std::vector<uint8_t>(scene_length);
		for(auto i = 0; i < scene_length; ++i)
		{
			scene_vec.push_back(in_scene[i].Byte);
		}

		// create the object image from byte data and also create a grayscale version of it
		const cv::Mat img_object(obj_h, obj_w, CV_8UC4, obj_vec.begin()._Ptr);
		cv::Mat gray_obj;
		cv::cvtColor(img_object, gray_obj, cv::COLOR_BGR2GRAY);

		// unsure if this will work since we're storing it in the same place we're reading it from
		// this will flip the image but it might be incorrect in the first place, will have to look over
		cv::flip(gray_obj, gray_obj, 0);

		// create the scene image from byte data and also create a grayscale version of it
		const cv::Mat img_scene(scene_h, scene_w, CV_8UC4, scene_vec.begin()._Ptr);
		cv::Mat grayScene;
		cv::cvtColor(img_scene, grayScene, cv::COLOR_BGR2GRAY);

		// unsure if this will work since we're storing it in the same place we're reading it from
		// this will flip the image but might be incorrect, will have to look over
		cv::flip(grayScene, grayScene, 0);

		// step 1: detect the keypoints and extract descriptors using SURF
		auto detector = cv::xfeatures2d::SURF::create(500);

		std::vector<cv::KeyPoint> keypoints_object, keypoints_scene;
		cv::Mat descriptors_object, descriptors_scene;

		detector->detectAndCompute(gray_obj, cv::Mat(), keypoints_object, descriptors_object);
		detector->detectAndCompute(grayScene, cv::Mat(), keypoints_scene, descriptors_scene);

		// step 2: Matching descriptor vectors using FLANN matcher
		cv::FlannBasedMatcher matcher;
		std::vector<cv::DMatch> matches;

		matcher.match(descriptors_object, descriptors_scene, matches);
		double max_dist = 0; double min_dist = 100;

		// quick calculation of max and min distance between keypoints
		for (auto i = 0; i < descriptors_object.rows; i++)
		{
			double dist = matches[i].distance;
			if (dist < min_dist) min_dist = dist;
			if (dist > max_dist) max_dist = dist;
		}

		// draw only good matches (i.e. whose distance is less than 3*min_dist
		std::vector<cv::DMatch> good_matches;
		for (auto i = 0; i < descriptors_object.rows; i++)
		{
			if (matches[i].distance < 3 * min_dist)
			{
				good_matches.push_back(matches[i]);
			}
		}

		// localize the object
		std::vector<cv::Point2f> obj;
		std::vector<cv::Point2f> scene;

		for (auto& match : good_matches)
		{
			// get the keypoints from the good matches
			obj.push_back(keypoints_object[match.queryIdx].pt);
			scene.push_back(keypoints_scene[match.trainIdx].pt);
		}

		if (!obj.empty() && !scene.empty())
		{
			auto h = cv::findHomography(obj, scene, cv::RANSAC);

			// get the corners of the image_1 (the object to be "detected")
			std::vector<cv::Point2f> obj_corners(4);
			obj_corners[0] = cv::Point(0, 0);
			obj_corners[1] = cv::Point(gray_obj.cols, 0);
			obj_corners[2] = cv::Point(gray_obj.cols, gray_obj.rows);
			obj_corners[3] = cv::Point(0, gray_obj.rows);

			std::vector<cv::Point2f> scene_corners(4);
			cv::perspectiveTransform(obj_corners, scene_corners, h);

			// Update3: These are correct if you are using top right as 0,0 (which you should)
			// these might all have to change when we are using a fullscreen image but we will see
			out_points[0] = RectanglePoints(
				scene_corners[0].x, scene_corners[0].y,
				scene_corners[1].x, scene_corners[1].y,
				scene_corners[2].x, scene_corners[2].y,
				scene_corners[3].x, scene_corners[3].y
			);
			return true;
			// change signature so we have a bool, true if we detected, false if we didn't
		}
		return false;
	}
}
