#include <VPXWrapper.hpp>
#include <vpx/vpx_decoder.h>
#include <stdexcept>


namespace VPXWrapper
{
	Decoder::Decoder(int width, int height) : width_{width}, height_{height}
	{
		auto cfg = vpx_codec_dec_cfg_t{static_cast<unsigned int>(width), static_cast<unsigned int>(height), 1};
		if(vpx_codec_dec_init(&codec_, decoder_, &cfg, 0))
		{
			throw std::runtime_error{"Could not init vpx codec"};
		}
	}


	Decoder::~Decoder()
	{
		vpx_codec_destroy(&codec_);
	}


	void bytes_from_image(vpx_image* img, uint8_t* out)
	{
		const auto width = img->d_w;
		const auto height = img->d_h;
		const auto total_size = width * height;

		for(auto y = 0; y < height; ++y)
		{
			for(auto x = 0; x < width; ++x)
			{
				out[y * width + x] = img->planes[0][y * img->stride[0] + x];
				if(x % 2 == 0 && y %2 == 0)
				{
					out[total_size + (y / 2) * (width / 2) + x / 2] = img->planes[1][y / 2 * img->stride[1] + (x / 2)];
					out[total_size + total_size / 4 + (y / 2) * (width / 2) + x / 2] = img->planes[2][y / 2 * img->stride[2] + (x / 2)];
				}
			}
		}
	}


	bool Decoder::decode_frame(const uint8_t* data, size_t size, uint8_t* out)
	{
		vpx_image* frame = nullptr;
		vpx_codec_iter_t iter = nullptr;
		vpx_image_t* img = nullptr;
		if (vpx_codec_decode(&codec_, data, size, nullptr, VPX_DL_REALTIME))
		{
			throw std::runtime_error{"PROBLEMS"};
		}
		while((img = vpx_codec_get_frame(&codec_, &iter)) != nullptr)
		{
			frame = img;
		}
		if (frame)
		{
			bytes_from_image(frame, out);
			return true;
		}
		return false;
	}


	int Decoder::width() const
	{
		return width_;
	}


	int Decoder::height() const
	{
		return height_;
	}







	Encoder::Encoder(int width, int height, int frame_rate) : width_{ width }, height_{ height }
	{
		auto cfg = vpx_codec_enc_cfg_t{};
		if (vpx_codec_enc_config_default(encoder_, &cfg, 0))
		{
			throw std::runtime_error{ "Could not get default config" };
		}
		cfg.g_w = static_cast<unsigned int>(width);
		cfg.g_h = static_cast<unsigned int>(height);
		cfg.g_timebase.num = 1;
		cfg.g_timebase.den = frame_rate;

		if (vpx_codec_enc_init(&codec_, encoder_, &cfg, 0))
		{
			throw std::runtime_error{ "Could not initialize codec" };
		}
	}

	Encoder::~Encoder()
	{
		auto iter = vpx_codec_iter_t{};
		do
		{
			vpx_codec_encode(&codec_, nullptr, frame_++, 1, 0, VPX_DL_REALTIME);
		} while (vpx_codec_get_cx_data(&codec_, &iter));
		vpx_codec_destroy(&codec_);
	}

	void Encoder::encode_frame(std::vector<uint8_t>& data)
	{
		auto img = vpx_image_t{};
		if (!vpx_img_wrap(&img, VPX_IMG_FMT_I420, static_cast<unsigned int>(width_), static_cast<unsigned int>(height_), 0, data.data()))
		{
			throw std::runtime_error{ "Could not wrap image" };
		}

		if (vpx_codec_encode(&codec_, &img, frame_++, 1, 0, VPX_DL_REALTIME))
		{
			throw std::runtime_error{ "Could not encode data" };
		}
		codec_iter_ = nullptr;
	}

	std::pair<uint8_t*, size_t> Encoder::get_next_frame()
	{
		const vpx_codec_cx_pkt_t* pkt = nullptr;
		while ((pkt = vpx_codec_get_cx_data(&codec_, &codec_iter_)) != nullptr)
		{
			if (pkt->kind == VPX_CODEC_CX_FRAME_PKT)
			{
				return std::pair<uint8_t*, size_t>{static_cast<uint8_t*>(pkt->data.frame.buf), pkt->data.frame.sz};
			}
		}

		return std::pair<uint8_t*, size_t>{nullptr, 0};
	}
}
