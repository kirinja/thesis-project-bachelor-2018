#include "VPXWrapper.hpp"
#include <memory>
#include "Structs.hpp"
#include "SURFDetection.hpp"
#include "ChessboardDetection.hpp"
#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videostab/ring_buffer.hpp>


using namespace std::string_literals;


std::unique_ptr<VPXWrapper::Decoder> decoder{};
std::unique_ptr<ChessboardDetection::Detector> chessboard_detector{};
std::unique_ptr<ChessboardDetection::Camera_calibrator> camera_calibrator{};


extern "C" void __declspec(dllexport) _stdcall InitVPX(int width, int height)
{
	decoder = std::make_unique<VPXWrapper::Decoder>(width, height);
}


extern "C" bool __declspec(dllexport) _stdcall GetDecodedFrame(uint8_t* encodedData, int encodedDataSize, uint8_t* decodedData)
{
	return decoder->decode_frame(encodedData, encodedDataSize, decodedData);
}


extern "C" void __declspec(dllexport) _stdcall SURF_Image_Recognition(ByteData* inScene, int sceneW, int sceneH, int sceneLength,
	ByteData* inObject, int objW, int objH, int objectLength, RectanglePoints* outPoints, float& frameTime)
{
	SURFDetection::surf_image_recognition(inScene, sceneW, sceneH, sceneLength, inObject, objW, objH, objectLength, outPoints);
}


struct Vector3Data
{
	float x, y, z;
};


void convert_vector(const std::vector<float>& in, Vector3Data& out)
{
	out.x = in[0];
	out.y = in[1];
	out.z = in[2];
}


extern "C" void __declspec(dllexport) _stdcall InitChessboardRecognition(int gridWidth, int gridHeight, float squareSize)
{
	cv::Mat camera_matrix;
	cv::Mat distortion_coefficients;

	auto fs = cv::FileStorage{ "calib_data.yml"s, cv::FileStorage::READ };
	fs["camera_matrix"] >> camera_matrix;
	fs["distortion_coefficients"] >> distortion_coefficients;
	fs.release();

	chessboard_detector = std::make_unique<ChessboardDetection::Detector>(camera_matrix, distortion_coefficients, cv::Size{ gridWidth, gridHeight }, squareSize);
}


extern "C" bool __declspec(dllexport) _stdcall DetectChessboard(uint8_t* imageData, int width, int height, Vector3Data& rotationVector, Vector3Data& translationVector, bool flipY)
{
	// Could optimize this by taking in ARGB or RGBA data and then copying it into a BGR Mat
	auto img = cv::Mat{ height, width, CV_8UC3, imageData };
	if (flipY)
	{
		cv::flip(img, img, 0);
	}

	auto rot = cv::Mat{};
	auto trans = cv::Mat{};
	if(chessboard_detector->detect(img, rot, trans))
	{
		convert_vector(rot, rotationVector);
		convert_vector(trans, translationVector);
		return true;
	}
	return false;
}


extern "C" void __declspec(dllexport) _stdcall InitCameraCalibration()
{
	camera_calibrator = std::make_unique<ChessboardDetection::Camera_calibrator>();
}


extern "C" void __declspec(dllexport) _stdcall SendCalibrationImage(uint8_t* imageData, int width, int height, bool flipY)
{
	// Could optimize this by taking in ARGB or RGBA data and then copying it into a BGR Mat
	auto img = cv::Mat{ height, width, CV_8UC3, imageData }.clone();
	if (flipY)
	{
		cv::flip(img, img, 0);
	}
	camera_calibrator->add_image(std::move(img));
}


extern "C" void __declspec(dllexport) _stdcall CalibrateCamera(int gridWidth, int gridHeight, float squareSize)
{
	camera_calibrator->calibrate({ gridWidth, gridHeight, }, squareSize);
}
