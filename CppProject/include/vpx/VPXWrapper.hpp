#pragma once

#include <vpx/vpx_codec.h>
#include <vpx/vp8dx.h>
#include <vpx/vp8cx.h>
#include <vector>


namespace VPXWrapper
{
	class Decoder
	{
	public:
		Decoder(int width, int height);
		~Decoder();

		bool decode_frame(const uint8_t* data, size_t size, uint8_t* out);

		int width() const;
		int height() const;

	private:
		vpx_codec_ctx_t codec_{};
		vpx_codec_iface_t* decoder_{ vpx_codec_vp9_dx() };

		int width_, height_;
	};




	class Encoder
	{
	public:
		Encoder(int width, int height, int frame_rate);
		~Encoder();

		void encode_frame(std::vector<uint8_t>& data);
		std::pair<uint8_t*, size_t> get_next_frame();

	private:
		vpx_codec_ctx_t codec_{};
		vpx_codec_iface_t* encoder_{ vpx_codec_vp9_cx() };
		vpx_codec_iter_t codec_iter_{ nullptr };
		int width_, height_;
		int frame_{};
	};
}
