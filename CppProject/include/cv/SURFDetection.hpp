#pragma once

struct ByteData;
struct RectanglePoints;


namespace SURFDetection
{
	bool surf_image_recognition(ByteData* in_scene, int scene_w, int scene_h, int scene_length,
		ByteData* in_object, int obj_w, int obj_h, int object_length, RectanglePoints* out_points);
}
