#pragma once
#include <opencv2/core/mat.hpp>


namespace ChessboardDetection
{
	class Detector
	{
	public:
		Detector(cv::Mat camera_matrix, cv::Mat distortion_coefficients, cv::Size chessboard_inner_corners, float square_size);

		bool detect(const cv::Mat& image, cv::Mat& rotation_vector, cv::Mat& translation_vector) const;

		const cv::Mat& camera_matrix() const;
		const cv::Mat& distortion_coefficients() const;

	private:
		cv::Mat camera_matrix_;
		cv::Mat distortion_coefficients_;
		cv::Size board_size_;
		std::vector<cv::Point3f> object_points_{};
	};


	class Camera_calibrator
	{
	public:
		void add_image(cv::Mat image);
		void calibrate(cv::Size chessboard_inner_corners, float square_size);

	private:
		std::vector<cv::Mat> images_{};
	};
}
