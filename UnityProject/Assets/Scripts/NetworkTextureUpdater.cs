﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class NetworkTextureUpdater : MonoBehaviour
{
#region Variables
#region Serialized

    public int Width = 128;
    public int Height = 128;
    public int Port = 12345;

#endregion

    private bool running_;
    private Thread networkThread_;
    private readonly object bufferLock_ = new object();

    private Color32[] buffer_;
    private Texture2D texture_;

#endregion


#region Unity methods

    private void OnEnable()
    {
        LibVPXInterop.InitVPX();
        
        texture_ = new Texture2D(Width, Height);
        GetComponent<Renderer>().material.mainTexture = texture_;

        lock (bufferLock_)
        {
            buffer_ = new Color32[Width * Height];
        }
        
        networkThread_ = new Thread(ReceivePackets);
        networkThread_.Start();
    }


    private void OnDisable()
    {
        running_ = false;
        networkThread_.Join();
    }


    private void Update()
    {
        lock (bufferLock_)
        {
            texture_.SetPixels32(buffer_);
            texture_.Apply();
        }
    }
    
#endregion


#region Private

    private void ReceivePackets() 
    {
        var receivingClient = new UdpClient(Port) {Client = {ReceiveTimeout = 200}};
        var remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);

        var packets = 0;
        var latestFrame = -1;
        var latestPacket = -1;
        var bytes = new byte[Width * Height * 3 / 2];

        const int packetSizeLimit = 508;
        const int headerSize = 12;
        const int dataSizeLimit = packetSizeLimit - headerSize;

        running_ = true; 
        while (running_) 
        { 
            try 
            { 
                var receiveBytes = receivingClient.Receive(ref remoteEndPoint);
                var frame = BitConverter.ToInt32(receiveBytes, 0);
                var packet = BitConverter.ToInt32(receiveBytes, 4);
                
                if (frame > latestFrame)
                {
                    latestFrame = frame;
                    latestPacket = -1;
                    packets = (int) Math.Ceiling(bytes.Length / (double) dataSizeLimit);
                }
                if(frame == latestFrame && packet == latestPacket + 1)
                {
                    latestPacket = packet;
                    Array.Copy(receiveBytes, headerSize, bytes, packet * dataSizeLimit, receiveBytes.Length - headerSize);
                    if (packet == packets - 1)
                    {
                        lock (bufferLock_)
                        {
                            Utils.YUV420ToColors32(bytes, buffer_, Width, Height);
                        }
                    }
                }
            } 
            catch (Exception e) 
            { 
                Debug.Log(e); 
            } 
        } 
    }

#endregion
}