﻿using System.Diagnostics;
using System.Threading;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class ThreadingPerformanceTest : MonoBehaviour
{
    private Thread thread_;
    private bool running_;

    private readonly Stopwatch watch_ = new Stopwatch();

    private void OnEnable()
    {
        thread_ = new Thread(RunLoop);
        thread_.Start();
        watch_.Start();
    }

    private void OnDisable()
    {
        watch_.Stop();
        running_ = false;
        if (thread_.IsAlive)
        {
            thread_.Join();
        }
    }
	
	// Update is called once per frame
	private void Update ()
	{
	    Debug.Log("Unity: " + 1000.0 / watch_.ElapsedMilliseconds);
	    watch_.Reset();
	    watch_.Start();
	}

    private void RunLoop()
    {
        running_ = true;
        var sw = new Stopwatch();
        sw.Start();
        while (running_)
        {
            Debug.Log("Threaded: " + 1000.0 / sw.ElapsedMilliseconds);
            sw.Reset();
            sw.Start();
        }
        sw.Stop();
    }
}
