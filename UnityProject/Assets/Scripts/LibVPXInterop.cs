﻿using System.Runtime.InteropServices;

public static class LibVPXInterop
{
    [DllImport("CppProject")]
    internal static extern void InitVPX();

    [DllImport("CppProject")]
    internal static extern unsafe bool GetDecodedFrame(byte* encodedData, int encodedDataSize, byte* decodedData);
}