﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class OpenCVFaceDetection : MonoBehaviour
{
    public static List<Vector2> NormalizedFacePositions = new List<Vector2>();// { get; private set; }
    public static Vector2 CameraResolution;

    /// <summary>
    /// Downscale factor to speed up detection.
    /// </summary>
    private const int DetectionDownScale = 1;

    private bool _ready;
    private int _maxFaceDetectCount = 5;
    private CvCircle[] _faces;

    public Material material;

    private CvByteData[] _bytes;
    private int _length;
    private int _width;
    private int _height;

    //public Texture2D Texture;
    public RenderTexture RenderTexture;

    void Start()
    {
        int camWidth = 0, camHeight = 0;
        int result = OpenCVInterop.Init(ref camWidth, ref camHeight);
        if (result < 0)
        {
            if (result == -1)
            {
                Debug.LogWarningFormat("[{0}] Failed to find cascades definition.", GetType());
            }
            else if (result == -2)
            {
                Debug.LogWarningFormat("[{0}] Failed to open camera stream.", GetType());
            }

            return;
        }

        CameraResolution = new Vector2(camWidth, camHeight);
        _faces = new CvCircle[_maxFaceDetectCount];
        NormalizedFacePositions = new List<Vector2>();
        OpenCVInterop.Set_Scale(DetectionDownScale);
        _ready = true;

        
        //Texture2D tex = new Texture2D(Texture.width, Texture.height, TextureFormat.BGRA32, false);
        //// we're creating a new Texture that is the size of the previous one, but we dont assign the image to it

        //// here we copy the pixels from the compressed image to our new Texture
        //tex.SetPixels32(Texture.GetPixels32());
        //tex.Apply();

        //var data = tex.GetRawTextureData();
        
        //_bytes = new CvByteData[data.Length];
        //_length = data.Length;
        //_width = tex.width;
        //_height = tex.height;

        ////Debug.Log(Texture.format + " - " + tex.format + " - " + data.Length + " - " + Texture.GetRawTextureData().Length + " | " + _width + " : " + _height);

        //// we can send bgra32 data to the opencv but we cant get the image data from our Texture correctly
        //for (int i = 0; i < data.Length; i++)
        //{
        //    _bytes[i].Byte = data[i];
        //}
    }
    
    void OnApplicationQuit()
    {
        if (_ready)
        {
            OpenCVInterop.Close();
        }
    }
    

    void Update()
    {
        if (!_ready)
            return;

        _bytes = Utils.CreateBytesFromRT(RenderTexture, out _length, out _width, out _height);

        unsafe
        {
            fixed (CvByteData* inData = _bytes)
            {
                OpenCVInterop.Send_ByteData(inData, _width, _height, _length);
            }
        }

        int detectedFaceCount = 0;
        unsafe
        {
            fixed (CvCircle* outFaces = _faces)
            {
                OpenCVInterop.Detect(outFaces, _maxFaceDetectCount, ref detectedFaceCount);
            }
        }

        NormalizedFacePositions.Clear();
        for (int i = 0; i < detectedFaceCount; i++)
        {
            NormalizedFacePositions.Add(new Vector2((_faces[i].X * DetectionDownScale) / CameraResolution.x, 1f - ((_faces[i].Y * DetectionDownScale) / CameraResolution.y)));
        }

        // get the image data from OpenCV C++ project (flipped) and apply it to a 2d Texture
        IntPtr intPtr;
        int size, width, height;
        OpenCVInterop.Process_Frame(out intPtr, out size, out width, out height);
        
        byte[] data = new byte[size];
        Marshal.Copy(intPtr, data, 0, size);

        //Debug.Log(size + " - " + intPtr + " - " + data.Length);

        // this might be the problem
        Texture2D tex = new Texture2D(width, height, TextureFormat.BGRA32, false);
        tex.LoadRawTextureData(data);
        tex.Apply();

        material.mainTexture = tex;
    }

}

// Define the functions which can be called from the .dll.
//internal static class OpenCVInterop
//{
//    [DllImport("OpenCVProject")]
//    internal static extern int Init(ref int outCameraWidth, ref int outCameraHeight);

//    [DllImport("OpenCVProject")]
//    internal static extern int Close();

//    [DllImport("OpenCVProject")]
//    internal static extern int SetScale(int downscale);

//    [DllImport("OpenCVProject")]
//    internal unsafe static extern void Detect(CvCircle* outFaces, int maxOutFacesCount, ref int outDetectedFacesCount);

//    [DllImport("OpenCVProject")]
//    internal static extern void ProcessFrame(out IntPtr data, out int size, out int width, out int height);

//    [DllImport("OpenCVProject")]
//    internal unsafe static extern void SendByteData(CvByteData* inData, int width, int height, int dataLength);
//}

