﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Debug = UnityEngine.Debug;

public class OpenCVSURFDetection : MonoBehaviour
{
    public Texture2D SceneImage;
    public Texture2D ObjectImage;
    public Camera Camera;
    public RenderTexture rt;

    private CvRectanglePoints[] _points;
    private CvRectanglePoints _previousFramePoints;
    private CvRectanglePoints _currentFramePoints;

    public Image[] PointsInScreenSpace;

    public GameObject[] PointsInSpace;

    [Tooltip("How many times we should do SURF calculations per second")]
    public float UpdatesPerSecond = 1.0f;
    private Timer _timer;

    private CvByteData[] _sceneData, _objData;
    private int _sceneLength, _sceneW, _sceneH, _objLength, _objW, _objH;

    private Thread _thread;

    private int _counter = 0;
    private bool _threadRunning;
    
    public void CreateTextureData(Texture2D Texture, out int length, out int width, out int height, out CvByteData[] bytes)
    {
        Texture2D tex = new Texture2D(Texture.width, Texture.height, TextureFormat.BGRA32, false);
        // we're creating a new Texture that is the size of the previous one, but we dont assign the image to it

        // here we copy the pixels from the compressed image to our new Texture
        tex.SetPixels32(Texture.GetPixels32());
        tex.Apply();

        var data = tex.GetRawTextureData();

        bytes = new CvByteData[data.Length];
        length = data.Length;
        width = tex.width;
        height = tex.height;
        for (int i = 0; i < data.Length; i++)
        {
            bytes[i].Byte = data[i];
        }
    }

    // Use this for initialization
    void Start ()
    {
        _timer = new Timer(1/UpdatesPerSecond);
        
        // we will have to get the texture data from the scene at set intervals but not from the object itself since it is static
        CreateTextureData(ObjectImage, out _objLength, out _objW, out _objH, out _objData);
        _sceneData = Utils.CreateBytesFromRT(rt, out _sceneLength, out _sceneW, out _sceneH);
        _points = new CvRectanglePoints[1];
        

        _thread = new Thread(Calcs);
        _thread.Start();
    }

    // need to look over threading
    void Calcs()
    {
        // we cant have other OpenCV calls while this thread is running, it will crash the application

        _threadRunning = true;
        bool workDone = false;
        System.Diagnostics.Stopwatch stopwatch = new Stopwatch();
        stopwatch.Start();

        // this crashes if we do more OpenCV calls. We are locked into doing all our OpenCV calls in here (or maybe we can only do one call because of this thread locking something)
        while (_threadRunning && !workDone)
        {
            if (stopwatch.Elapsed.Seconds >= UpdatesPerSecond)
            {
                // in here we do the calculations
                float frametime = 0;

                unsafe
                {
                    fixed (CvByteData* inScene = _sceneData)
                    fixed (CvByteData* inObj = _objData)
                    fixed (CvRectanglePoints* outPoiubts = _points)
                    {
                        OpenCVInterop.SURF_Image_Recognition(inScene, _sceneW, _sceneH, _sceneLength, inObj, _objW, _objH, _objLength, outPoiubts, ref frametime);
                    }
                }
                _counter++;
                stopwatch.Reset();
                stopwatch.Start();
            }
        }
        _threadRunning = false;
    }

    private void OnApplicationQuit()
    {
        if (_threadRunning)
        {
            // This forces the while loop in the ThreadedWork function to abort.
            _threadRunning = false;

            // This waits until the thread exits,
            // ensuring any cleanup we do after this is safe. 
            _thread.Join();
        }
    }

    // Update is called once per frame
    void Update ()
	{
	    _timer.Update(Time.deltaTime);
	    if (_timer.IsDone)
	    {
            // update the sceneData every so often (this is not very optimized)
	        _sceneData = Utils.CreateBytesFromRT(rt, out _sceneLength, out _sceneW, out _sceneH);
            
            _currentFramePoints = _points[0];
            _previousFramePoints = _points[0];

            _timer.Reset();
        }

        // these are gonna be changed to not using _points but rather currentPoint
	    var point1 = new Vector3(_points[0].x1, -_points[0].y1, 0);
        var point2 = new Vector3(_points[0].x2, -_points[0].y2, 0);
        var point3 = new Vector3(_points[0].x3, -_points[0].y3, 0);
        var point4 = new Vector3(_points[0].x4, -_points[0].y4, 0);

        PointsInScreenSpace[0].rectTransform.anchoredPosition = point1;
        PointsInScreenSpace[1].rectTransform.anchoredPosition = point2;
        PointsInScreenSpace[2].rectTransform.anchoredPosition = point3;
        PointsInScreenSpace[3].rectTransform.anchoredPosition = point4;

        var p1 = new Vector3(_points[0].x1, Camera.main.pixelHeight - _points[0].y1, -Camera.transform.position.z);
        var cp1 = Camera.ScreenToWorldPoint(p1);
        Debug.Log(p1 + " : " + cp1);

        PointsInSpace[0].transform.position = cp1;
        PointsInSpace[1].transform.position =
            Camera.main.ScreenToWorldPoint(new Vector3(_points[0].x2, Camera.main.pixelHeight - _points[0].y2, -Camera.transform.position.z));
        PointsInSpace[2].transform.position =
            Camera.main.ScreenToWorldPoint(new Vector3(_points[0].x3, Camera.main.pixelHeight - _points[0].y3, -Camera.transform.position.z));
        PointsInSpace[3].transform.position =
            Camera.main.ScreenToWorldPoint(new Vector3(_points[0].x4, Camera.main.pixelHeight - _points[0].y4, -Camera.transform.position.z));
        
    }

    // todo:
    // if we cant find the object then dont show the image/dots

    // todo:
    // we need to consturct a rectangle with the rotation and position of the point data
    // we want to use this rectangle to draw some stuff on
    // another solution might be to have a 3d model and have the "screen" on it and change rotation and position depending on point data
    // 3d model is slightly harder to implement due to the fact that it needs to take into account rotation in 3d space and also changing position in 3d space is hard
    // we will most likely need to do a 3d model implementation since it's most natural when in VR
}
