﻿using System.Threading;
using UnityEngine;

public class OpenCVCameraCalibrator : MonoBehaviour 
{
#region Events

    public delegate void PictureTaken(int pics);
    public event PictureTaken OnPhotoTaken = delegate { };

#endregion


#region Variables
#region Serialized
    
    [SerializeField] public int GridWidth = 5;
    [SerializeField] public int GridHeight = 9;
    [SerializeField] public float SquareSize = 0.0084f;
    [SerializeField] public bool UseVive = true;
    [SerializeField] public bool FlipWebCamY;

#endregion
    
    private bool useVive_;
    private WebCamTexture webCamTex_;
    private SteamVR_TrackedCamera.VideoStreamTexture viveCamSource_;

    private int images_;
    private Thread calibrationThread_;

#endregion
    

#region Properties

    public Texture2D CameraTexture { get; private set; }
    
#endregion


#region Unity methods
    
    private void OnEnable()
    {
        useVive_ = UseVive;

        if (useVive_)
        {
            viveCamSource_ = SteamVR_TrackedCamera.Source(false);
            viveCamSource_.Acquire();
        }
        else
        {
            webCamTex_ = new WebCamTexture();
            webCamTex_.Play();
            CameraTexture = new Texture2D(webCamTex_.width, webCamTex_.height);
        }

        OpenCVInterop.InitCameraCalibration();
        calibrationThread_ = new Thread(DoCalibration);
    }


    private void OnDisable()
    {
        if (useVive_)
        {
            viveCamSource_.Release();
        }
        else
        {
            webCamTex_.Stop();
        }

        if (calibrationThread_.IsAlive)
        {
            calibrationThread_.Join();
        }
    }


    private void Update()
    {
        if (useVive_)
        {
            CameraTexture = viveCamSource_.texture;
        }
        else
        {
            CameraTexture.SetPixels32(webCamTex_.GetPixels32());
            CameraTexture.Apply();
        }
    }

#endregion


#region Public

    public void TakePhoto()
    {
        var rt = RenderTexture.GetTemporary(CameraTexture.width, CameraTexture.height);
        Graphics.Blit(CameraTexture, rt);
        var tempTex = Utils.RenderTextureToTexture2D(rt);
        RenderTexture.ReleaseTemporary(rt);

        var bytes = Utils.Texture2DToBGRBytes(tempTex);
      
        unsafe
        {
            fixed (byte* img = bytes)
            {
                OpenCVInterop.SendCalibrationImage(img, CameraTexture.width, CameraTexture.height, FlipWebCamY);
            }
        }

        OnPhotoTaken(++images_);
    }


    public void Calibrate()
    {
        calibrationThread_.Start();
    }

#endregion


#region Private

    private void DoCalibration()
    {
        OpenCVInterop.CalibrateCamera(GridWidth, GridHeight, SquareSize);
    }

#endregion
}
