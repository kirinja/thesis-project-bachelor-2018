﻿using System;
using System.Runtime.InteropServices;

public static class OpenCVInterop
{
    #region FACE DETECTION
    [DllImport("OpenCVProject")]
    internal static extern int Init(ref int outCameraWidth, ref int outCameraHeight);

    [DllImport("OpenCVProject")]
    internal static extern void Close();

    [DllImport("OpenCVProject")]
    internal static extern int Set_Scale(int downscale);

    [DllImport("OpenCVProject")]
    internal static extern unsafe void Detect(CvCircle* outFaces, int maxOutFacesCount, ref int outDetectedFacesCount);
    #endregion

    #region SEND IMAGE TO OPENCV AND BACK
    [DllImport("OpenCVProject")]
    internal static extern void Process_Frame(out IntPtr data, out int size, out int width, out int height);

    [DllImport("OpenCVProject")]
    internal static extern unsafe void Send_ByteData(CvByteData* inData, int width, int height, int dataLength);
    #endregion

    #region IMAGE DETECTION
    [DllImport("CppProject")]
    internal static extern unsafe void SURF_Image_Recognition(CvByteData* inScene, int sceneW, int sceneH, int sceneLength,
        CvByteData* inObject, int objW, int objH, int objectLength, CvRectanglePoints* outPoints, ref float outFrametime);

    [DllImport("CppProject")]
    internal static extern void InitChessboardRecognition(int gridWidth, int gridHeight, float squareSize);

    [DllImport("CppProject")]
    internal static extern unsafe bool DetectChessboard(byte* imageData, int width, int height,
        ref Vector3Data rotationVector, ref Vector3Data translationVector, bool flipY);

    [DllImport("CppProject")]
    internal static extern void InitCameraCalibration();

    [DllImport("CppProject")]
    internal static extern unsafe void SendCalibrationImage(byte* imageData, int width, int height, bool flipY);

    [DllImport("CppProject")]
    internal static extern void CalibrateCamera(int gridWidth, int gridHeight, float squareSize);

#endregion
}
