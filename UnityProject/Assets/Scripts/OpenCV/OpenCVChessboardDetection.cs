﻿using System.Threading;
using UnityEngine;

public class OpenCVChessboardDetection : MonoBehaviour
{
#region Variables
#region Serialized
    
    [SerializeField] public int GridWidth = 5;
    [SerializeField] public int GridHeight = 9;
    [SerializeField] public float SquareSize = 0.0084f;
    [SerializeField] public Renderer Renderer;
    [SerializeField] public bool FlipWebCamY;
    [SerializeField] public bool UseVive; // This variable should only be used in OnEnable

#endregion

    private bool useVive_;
    private SteamVR_TrackedCamera.VideoStreamTexture viveCamSource_;
    private WebCamTexture webCamTex_;

    private RenderTexture renderTexture_;
    private Texture2D bufTexture_;
    
    private Thread thread_;
    private bool threadRunning_;
    private readonly object textureLock_ = new object();
    private readonly object resultLock_ = new object();

    // Variables that detection thread is allowed to access
    private Color32[] buffer_;
    private int textureWidth_;
    private int textureHeight_;

    // Variables that detection thread is allowed to modify
    private bool foundObject_;
    private Vector3Data rotVector_;
    private Vector3Data transVector_;

    private byte[] bgrBytes_;
    
#endregion


#region Unity methods

    private void OnEnable()
    {
        useVive_ = UseVive;

        if (useVive_)
        {
            viveCamSource_ = SteamVR_TrackedCamera.Source(false);
            viveCamSource_.Acquire();
        }
        else
        {
            webCamTex_ = new WebCamTexture();
            webCamTex_.Play();
        }

        OpenCVInterop.InitChessboardRecognition(GridWidth, GridHeight, SquareSize);

        thread_ = new Thread(DetectionLoop);
        thread_.Start();
    }

    private void OnDisable()
    {
        if (renderTexture_)
        {
            DestroyTextures();
        }

        if (useVive_)
        {
            viveCamSource_.Release();
        }
        else
        {
            webCamTex_.Stop();
        }
        
        threadRunning_ = false;
        if (thread_.IsAlive)
        {
            thread_.Join();
        }
    }

    private void Update()
    {
        var camTex = useVive_ ? (Texture) viveCamSource_.texture : webCamTex_;

        if (camTex == null || camTex.width == 0)
        {
            return;
        }

        if (!renderTexture_)
        {
            InitTextures(camTex.width, camTex.height);
        }
        
        Graphics.Blit(camTex, renderTexture_);
        Utils.RenderTextureToTexture2D(renderTexture_, bufTexture_);

        lock (textureLock_)
        {
            buffer_ = bufTexture_.GetPixels32();
        }

        lock (resultLock_)
        {
            if (foundObject_)
            {
                ApplyVectors(rotVector_, transVector_);
            }

            Renderer.enabled = foundObject_;
        }
    }

#endregion
    
    
#region Private

    private void InitTextures(int width, int height)
    {
        renderTexture_ = RenderTexture.GetTemporary(width, height);
        bufTexture_ = new Texture2D(width, height);

        lock (textureLock_)
        {
            buffer_ = new Color32[width * height];
            bgrBytes_ = new byte[width * height * 3];
            textureWidth_ = width;
            textureHeight_ = height;
        }
    }

    private void DestroyTextures()
    {
        RenderTexture.ReleaseTemporary(renderTexture_);
        bufTexture_ = null;
        buffer_ = null;
    }

    private void DetectionLoop()
    {
        // this crashes if we do more OpenCV calls. We are locked into doing all our OpenCV calls in here (or maybe we can only do one call because of this thread locking something)
        threadRunning_ = true;
        while (threadRunning_)
        {
            lock (textureLock_)
            {
                if (buffer_ == null)
                {
                    continue;
                }
                Utils.Colors32ToBGRBytes(buffer_, bgrBytes_, textureWidth_, textureHeight_);
            }

            var rotVector = new Vector3Data();
            var transVector = new Vector3Data();
            bool found;
            unsafe
            {
                fixed (byte* img = bgrBytes_)
                {
                    found = OpenCVInterop.DetectChessboard(img, textureWidth_, textureHeight_, ref rotVector, ref transVector,
                        FlipWebCamY);
                }
            }

            lock (resultLock_)
            {
                foundObject_ = found;
                rotVector_ = rotVector;
                transVector_ = transVector;
            }
        }
    }

    private void ApplyVectors(Vector3Data rotVec, Vector3Data transformVec)
    {
        var theta = Mathf.Sqrt(rotVec.x * rotVec.x + rotVec.y * rotVec.y +
                               rotVec.z * rotVec.z) * 180 / Mathf.PI;
        var axis = new Vector3(-rotVec.x, rotVec.y, -rotVec.z);
        var rot = Quaternion.AngleAxis(theta, axis);

        transform.localRotation = rot;
        
        var angle = transform.localEulerAngles;
        angle.x = Utils.NormalizeAngle(angle.x);
        angle.y = Utils.NormalizeAngle(angle.y);
        angle.z = Utils.NormalizeAngle(angle.z);
        if (angle.z > 90 && angle.z < 270)
        {
            angle.z += 180f;
        }

        angle.z = Utils.NormalizeAngle(angle.z);
        transform.localEulerAngles = angle;

        transform.localPosition = new Vector3(transformVec.x, -transformVec.y, transformVec.z);
    }

#endregion
}
