﻿using System.Runtime.InteropServices;
using NUnit.Framework.Constraints;

// Define the structure to be sequential and with the correct byte size (3 ints = 4 bytes * 3 = 12 bytes)
[StructLayout(LayoutKind.Sequential, Size = 12)]
public struct CvCircle
{
    public int X, Y, Radius;
}

[StructLayout(LayoutKind.Sequential, Size = 1)]
public struct CvByteData
{
    public byte Byte;
}

[StructLayout(LayoutKind.Sequential, Size = 12)]
public struct Vector3Data
{
    public float x;
    public float y;
    public float z;
}

[StructLayout(LayoutKind.Sequential, Size = 32)]
public struct CvRectanglePoints
{
    public CvRectanglePoints(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
    {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;
        this.x4 = x4;
        this.y4 = y4;
    }
    // 4 sets of 2 floating points
    public float x1, y1;
    public float x2, y2;
    public float x3, y3;
    public float x4, y4;

    public static CvRectanglePoints operator -(CvRectanglePoints p1, CvRectanglePoints p2)
    {
        return new CvRectanglePoints(
            p1.x1 - p2.x1, p1.y1 - p2.y1,
            p1.x2 - p2.x2, p1.y2 - p2.y2,
            p1.x3 - p2.x3, p1.y2 - p2.y3,
            p1.x4 - p2.x4, p1.y2 - p2.y4);
    }
}
