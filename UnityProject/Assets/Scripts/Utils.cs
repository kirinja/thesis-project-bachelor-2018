﻿using UnityEngine;

public static class Utils
{
    public static Color32 YUV444ToColor32(byte y, byte u, byte v)
    {
        var r = Clamp(1192 * (y - 16) + 1634 * (v - 128), 0, 262143) >> 10;
        var g = Clamp(1192 * (y - 16) - 833 * (v - 128) - 400 * (u - 128), 0, 262143) >> 10;
        var b = Clamp(1192 * (y - 16) + 2066 * (u - 128), 0, 262143) >> 10;

        return new Color32((byte) r, (byte) g, (byte) b, byte.MaxValue);
    }


    public static int Clamp(int val, int min, int max)
    {
        return val > max ? max : (val < min ? min : val);
    }


    public static CvByteData[] CreateBytesFromRT(RenderTexture rt, out int length, out int width, out int height)
    {
        // create Texture from render image
        var rtTexture = RenderTextureToTexture2D(rt);

        // convert image to BGRA32 format (opencv need this, RGBa32 might work as well)
        Texture2D bgraTexture = new Texture2D(rtTexture.width, rtTexture.height, TextureFormat.BGRA32, false);

        // copy over pixels from rt Texture to bgra Texture
        bgraTexture.SetPixels32(rtTexture.GetPixels32());
        bgraTexture.Apply();

        // get the raw bytes that represent the image
        var rawBytes = bgraTexture.GetRawTextureData();

        // update the image variables
        length = rawBytes.Length;
        width = rtTexture.width;
        height = rtTexture.height;

        // create an array of the correct size
        var bytes = new CvByteData[rawBytes.Length];

        // assign the byte data to our array that we will send to opencv
        for (int i = 0; i < rawBytes.Length; i++)
        {
            bytes[i].Byte = rawBytes[i];
        }

        return bytes;
    }


    public static Texture2D RenderTextureToTexture2D(RenderTexture rt)
    {
        // Create a new Texture2D and read the RenderTexture image into it
        var tex = new Texture2D(rt.width, rt.height);
        RenderTextureToTexture2D(rt, tex);
        return tex;
    }


    public static void RenderTextureToTexture2D(RenderTexture rt, Texture2D t2D)
    {
        var oldRT = RenderTexture.active;
        RenderTexture.active = rt;

        t2D.ReadPixels(new Rect(0, 0, t2D.width, t2D.height), 0, 0);

        RenderTexture.active = oldRT;
    }


    public static byte[] Texture2DToBGRBytes(Texture2D tex)
    {
        var buf = new byte[tex.width * tex.height * 3];
        var colors = tex.GetPixels32();

        Colors32ToBGRBytes(colors, buf, tex.width, tex.height);

        return buf;
    }


    public static void Colors32ToBGRBytes(Color32[] colors, byte[] bytes, int width, int height)
    {
        for (var y = 0; y < height; ++y)
        {
            for (var x = 0; x < width; ++x)
            {
                var start = (y * width + x) * 3;
                var col = colors[y * width + x];
                bytes[start] = col.b;
                bytes[start + 1] = col.g;
                bytes[start + 2] = col.r;
            }
        }
    }


    public static void YUV420ToColors32(byte[] yuv, Color32[] colors, int width, int height)
    {
        var totalSize = width * height;
        for (var yPos = 0; yPos < height; ++yPos)
        {
            for (var xPos = 0; xPos < width; ++xPos)
            {
                var y = yuv[yPos * width + xPos];
                var u = yuv[yPos / 2 * (width / 2) + xPos / 2 + totalSize];
                var v = yuv[yPos / 2 * (width / 2) + xPos / 2 + totalSize + totalSize / 4];
                colors[yPos * width + (width - 1 - xPos)] = Utils.YUV444ToColor32(y, u, v);
            }
        }
    }


    public static float NormalizeAngle(float angle)
    {
        return angle % 360f;
    }
}
