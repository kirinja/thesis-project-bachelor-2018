﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class CameraPreview : MonoBehaviour
{
#region Variables
#region Serialized

    public OpenCVCameraCalibrator Calibrator;

#endregion
#endregion


#region Unity methods
    
    private void Update()
    {
        GetComponent<RawImage>().texture = Calibrator.CameraTexture;
    }

#endregion
}
