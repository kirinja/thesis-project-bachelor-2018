﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class CalibrationNumImages : MonoBehaviour
{
#region Variables
#region Serialized

    public OpenCVCameraCalibrator Calibrator;

#endregion

    private Text text_;
    
#endregion


#region Unity methods
    
    private void Start()
    {
        text_ = GetComponent<Text>();
        Calibrator.OnPhotoTaken += OnPhotoTaken;
    }

#endregion


#region Private

    private void OnPhotoTaken(int images)
    {
        text_.text = images.ToString() + " photos taken";
    }

#endregion
}
