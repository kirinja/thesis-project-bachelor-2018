﻿using UnityEngine;

public class ObjectRotator : MonoBehaviour
{
    public MeshFilter Input;
    public float Multiplier = 50f;


    private void Start()
    {
        var verts = new[]{new Vector3(-0.5f, -0.5f, 0f), new Vector3(0.5f, -0.5f, 0f), new Vector3(0.5f, 0.5f, 0f), new Vector3(-0.5f, 0.5f, 0f)};
        var UV = new Vector2[4];
        var tris = new[] {0, 1, 2, 0, 2, 3};
        var mesh = new Mesh
        {
            vertices = verts,
            uv = UV,
            triangles = tris
        };
        Input.mesh = mesh;

        var otherMesh = new Mesh
        {
            vertices = verts,
            uv = UV,
            triangles = tris
        };
        GetComponent<MeshFilter>().mesh = otherMesh;
    }
    
	
	// Update is called once per frame
    private void Update ()
	{
		SolveRotation(ScreenSpaceFromVertex(3), ScreenSpaceFromVertex(2), ScreenSpaceFromVertex(0), ScreenSpaceFromVertex(1));
	}

    private Vector2 ScreenSpaceFromVertex(int index)
    {
        return Camera.main.WorldToViewportPoint(Input.transform.TransformPoint(Input.mesh.vertices[index]));
    }

    private void SolveRotation(Vector2 topLeft, Vector2 topRight, Vector2 bottomLeft, Vector2 bottomRight)
    {
        var top = topRight - topLeft;
        var left = bottomLeft - topLeft;
        var bottom = bottomRight - bottomLeft;
        var right = bottomRight - topRight;

        transform.localEulerAngles = new Vector3((top.magnitude - bottom.magnitude) * Multiplier, (left.magnitude - right.magnitude) * Multiplier, 0f);
    }
}
